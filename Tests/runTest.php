<?php
/**
 *  
 * @author Muhammad Esmaelgee <muhammad.esmaelgee@redboxdigital.com>
 * @copyright Copyright (c) 2017 Redbox Digital (http://www.redboxdigital.com)
 *
 * Improvements
 * @TODO 
 *
 */



class runTest extends PHPUnit_Extensions_Selenium2TestCase
{
	/**
	 * session of current testsuite
	 * @var string
	 */
	protected $sid;

	/**
	 * Name of the OS being used
	 * @var string
	 */
	protected $os;
	
	/**
	 * Selector after extraction
	 * @var string
	 */
	protected $extractedSelector;
	
	
	/**
	 * Extracted selector of previous element
	 * @var string
	 */
	protected $oldExtractedSelector;
	/**
	 * determine whether http or https
	 * @var string
	 */
	protected $http;
	/**
	 * string to determine the price format
	 * @var string;
	 */
	protected $currencyFormat;
	
	/**
	 * Array holding all selector for current test
	 * @var ArrayAccess
	 */
	protected $currentSelectors = array();
	
	/**
	 * Array Holding all data sets
	 * @var ArrayAccess
	 */
	protected $dataSet = array();
	/**
	 * Path of alt folder for logging errors.
	 * @var String
	 */
	protected $reportAltPath;

    /**
     * Status of current test once run.
     * @var string
     */
    protected $testStatus;
    
    /**
     * Count total no of tests
     * @var int
     */
	protected $countTotalTest;
	
	/**
	 * Path of Unit Testcases w.r.t to Project Path
	 * @var string
	 */
	protected $unitTestCasesPath = DIRECTORY_SEPARATOR . "UnitTestcases" . DIRECTORY_SEPARATOR;
    
    /**
     * Count tests that have passed
     * @var int
     */
    protected $countPassedTest;

    /**
     * The Current Variation Index
     * @var Int
     */
    protected $currentVariationIndex;

    /**
     * The Current Variation Name
     * @var String
     */
    protected $currentVariationName;
    
    /**
     * the variation to be tested
     * @var string
     */
    protected $variation;
    /**
     * Counts the number of completed test.
     * @var int
     */
    protected $countCompletedTest;

    /**
     * Counts the number of terminated test.
     * @var int
     */
    protected $countTerminatedTest;    
    
    /**
     * Name of Test Suite
     * @var string
     */
    protected $testsuiteName;

    /**
     * @var string
     */
    protected $testcaseStepsForOutput;

    /**
     * Contains errors of Test scenarios
     * @var string
     */
    protected $errorForOutput;

    /**
     * @var string
     */
    protected $htaccess;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $adminurl;
    /**
     * @var string
     */
    protected $browser;

    /**
     * Contains all the path for each testcase
     * @var array
     */
    protected $allPath = array();

    /**
     * Only the path to the testcase folder of the current script
     * @var string
     */
    protected $path;
    
    /**
     * path to the testcase of the current script
     * @var string
     */
    protected $testCasePath;

    /**
     * Variable used to store testcases value.
     * @var array
     */
    protected $var = array();

    /**
     * Error log
     * @var array
     */
    protected $errorMessage = [];
    
    /**
     * Error log from mini TestCases;
     * @var String
     */
    protected $errorTestSteps = array();

    /**
     * Check Error
     * @var int
     */
    protected $error = 0;

    /**
     * Iterator
     * @var int
     */
    public $ii = 2;

    /**
     * Contain Current Test Case name.
     * @var String.
     */
    protected $testCase;

    /**
     * @var Array.
     */
    protected $testStep = [];

    /**
     * @var SimpleElement
     */
    protected $sxml;
    
    /**
     * @var value from files 
     */
	protected $value;

    /**
     * Read steps from Xml file
     */
    public function decomposeHtml(){

       //load file
        $this->sxml = $this->loadHtmlAsDom($this->testCasePath);
        $sxml = $this->sxml;
		$noVariation = 0;
        $i = $this->ii;
        
        //get test case name and variation
        $this->testCase = $sxml->xpath("/html/body/table/tbody/tr[" . $i . "]/td[1]/font");
        $this->testCase = (string)$this->testCase[0];
		$this->testCase = substr($this->testCase, 0, strpos($this->testCase, "|")-1);
        echo "\n\nExecuting TestCase: " . $this->testCase . "\n";
                
        ob_flush();

        $allSteps = [];
        $errors = [];
		
		
        //iterate through steps
        while(1){
            
            $currentNode = $sxml->xpath("/html/body/table/tbody/tr[{$i}]/td[2]/font");
            $currentStep = explode("|",$currentNode[0]);
            $currentStepText = $sxml->xpath("/html/body/table/tbody/tr[{$i}]/td[3]/font");

//             $currentNode = $sxml->xpath("/html/body/table/tbody/tr[{$i}]/td[2]/font/a/href");
			$currentVariation = $sxml->xpath("/html/body/table/tbody/tr[{$i}]/td[1]/font");
			$currentVariation = (string)$currentVariation[0];			
			
			if(isset($currentVariation) && $currentVariation !== ""){
				
				$this->error = 0; //re initialise error on each testcase
				$iterator = 1;
				
				$currentVariation = substr($currentVariation, strpos($currentVariation, "|")+2);
				
				$this->currentVariationIndex = substr($currentVariation, strpos($currentVariation,"Variation") + 10);
				$this->currentVariationIndex = substr($this->currentVariationIndex, 0, strpos($this->currentVariationIndex,"_"));				
				
				if($this->currentVariationIndex !== $this->variation){					
					$iterator=0;
				}else{
					$this->currentVariationName = $currentVariation;
					echo "\n\nExecuting Variation: " . $currentVariation . "\n";									
					$this->testcaseStepsForOutput[$this->testCase . 'variation' . $this->currentVariationIndex][] = "-Variation|" . $currentVariation;
					$errors[] = "<<<<<" . $this->testCase . 'variation' . $this->currentVariationIndex;
				}
				
				unset($currentVariation);
				$noVariation++;
			}

            if(strpos($currentStep[0],"EndTest") !== 0) {

                    if ($this->error > 0) {
                        $i++;
                        continue;
                    }
                    
                    if ($iterator === 0){
                    	$i++;
                    	continue;
                    }
                   

                    //get current steps action
                    $currentStepAction = $currentStep[0];
//                     $currentStepAction = strtolower($currentStepAction);
//                     $currentStepAction = str_replace(' ', '', $currentStepAction);
					
                    $currentStepAction = $this->formatInputs($currentStepAction);
                    
                    $currentStepSelector = $currentStep[1];

                    if (isset($currentStep[2])) {
                        $currentStepValue = $currentStep[2];
                    } else {
                        $currentStepValue = "";
                    }

                    if(isset($currentStepText[0])){
                       $currentStepStep = $currentStepText[0];
                    }else{
                        $currentStepStep="";
                    }
					                    
                    $errors[] = $this->executeScripts($currentStepAction, $currentStepSelector, $currentStepValue, $currentStepStep);

                    $i++;

                } else {
                    if ($this->error == 0) { }

                    echo "\n- Test Complete\n";
                    break;
                }
            }
            return array_filter($errors);
    }
    
    
    public function decomposeHtml2($testcase){
    
    	//load file
    	$this->sxml = $this->loadHtmlAsDom($testcase);
    	$miniSxml = $this->sxml;
    	$noVariation = 0;
    	$i = 2;
    
    	$allSteps = [];
    	$errors = [];
    
    	//iterate through steps
    	while(1){
    
    		$currentNode = $miniSxml->xpath("/html/body/table/tbody/tr[{$i}]/td[2]/font");
    		$currentStep = explode("|",$currentNode[0]);
    		$currentStepText = $miniSxml->xpath("/html/body/table/tbody/tr[{$i}]/td[3]/font");

    		if(strpos($currentStep[0],"EndTest") !== 0) {
    			
    			if ($this->error > 0) {
    				$i++;
    				continue;
    			}
    
    
    			//get current steps action
    			$currentStepAction = $currentStep[0];
    			//                     $currentStepAction = strtolower($currentStepAction);
    			//                     $currentStepAction = str_replace(' ', '', $currentStepAction);
    				
    			$currentStepAction = $this->formatInputs($currentStepAction);
    			
    			$currentStepSelector = $currentStep[1];
    
    			if (isset($currentStep[2])) {
    				$currentStepValue = $currentStep[2];
    			} else {
    				$currentStepValue = "";
    			}
    
    			if(isset($currentStepText[0])){
    				$currentStepStep = $currentStepText[0];
    			}else{
    				$currentStepStep="";
    			}
    			
    			$errors[] = $this->executeScripts($currentStepAction, $currentStepSelector, $currentStepValue, "\t".$currentStepStep);
    
    			$i++;
    
    		} else {
//     			if ($this->error == 0) { }
    			echo "\n\t- STEP - Completed\t";
//     			echo "\n- Test Complete\n";
    			break;
    		}
    	}
    	
    	return array_filter($errors);
    }

   
	
    
    /**
     * @return SimpleXMLElement
     */
    public function loadHtmlAsDom($testCasePath){
        //load files
        $file = $testCasePath;                
        $html = file_get_contents($file);       //reads file into $html
        $doc = new DOMDocument();               //Initialise Dom Doc
        $doc->loadHTML($html);                  //load variable as html
        return simplexml_import_dom($doc);     //load dom elements
    }

    
    
    /**
     * Extract following information from Testsuite: Name, path to testcases, browser, url and htaccess
     */
    protected function setUp(){    	    		
    	
    	$testsuite = simplexml_load_file("currentTest.xml") or die("Cannot open currentTest.xml");
    	
    	$this->sid = (string)$testsuite->xpath("//currentTestPlan/SID")[0];
    	$this->testsuiteName = (string)$testsuite->xpath("//currentTestPlan/testsuite")[0];
    	$testcases = (string)$testsuite->xpath("//currentTestPlan/testcase")[0];
    	$port = (string)$testsuite->xpath("//currentTestPlan/port")[0];
    	$this->http = (string)$testsuite->xpath("//currentTestPlan/http")[0];
        $this->path = (string)$testsuite->xpath("//currentTestPlan/path")[0];
        $this->browser = (string)$testsuite->xpath("//currentTestPlan/browser")[0];
        $this->url = (string)$testsuite->xpath("//currentTestPlan/url")[0];
        $this->adminurl = (string)$testsuite->xpath("//currentTestPlan/adminurl")[0];
        $this->htaccess = (string)$testsuite->xpath("//currentTestPlan/htaccess")[0];
        $this->currencyFormat = (string)$testsuite->xpath("//currentTestPlan/currencyFormat")[0];
        $reportAltPath = $testsuite->xpath("//currentTestPlan/reportAltPath");
        $this->variation = (string)$testsuite->xpath("//currentTestPlan/variation")[0]; 
              
        
        if(!empty($reportAltPath)){
        	$this->reportAltPath = DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . $reportAltPath[0];
        }else{
        	$this->reportAltPath = "";
        }

            array_push($this->allPath,$this->path . "/" . $testcases. ".html");

            $this->setBrowser($this->browser);            
            $this->setBrowserUrl('http://');
            $this->setSeleniumServerRequestsTimeout(300000);
            $this->setPort((int)$port);            
     }
     

    /**
    * @test
    */
     public function testRun()
    {
     	
    	$this->testcaseStepsForOutput = array();
    	
    	$this->countCompletedTest = 0;
    	$this->countPassedTest = 0;
    	$this->countTerminatedTest = 0;
		$this->countTotalTest = 0;
		
        foreach($this->allPath as $testcase){

            $this->prepareSession()->cookie()->clear();
            
            
            $this->error = 0;
            unset($this->var);
            $this->initialiseTestCaseVariables(); //initialise variables used in testcase
                        
            $this->countTotalTest++;
            $this->testStatus = " - was terminated ";

            $this->testCasePath = $testcase;
            
            $selectorFilePath = substr($this->testCasePath, 0, strrpos($this->testCasePath, "/"));
            
            $this->currentSelectors = simplexml_load_file($selectorFilePath . $this->unitTestCasesPath . "Selectors.xml") or die("Cannot open Selector.xml");
            
            $this->dataSet = simplexml_load_file($selectorFilePath . $this->unitTestCasesPath . "dataSets.xml");
            
            $errors = $this->decomposeHtml();
			
            file_put_contents("logger.txt",$this->testCase . "_variation_" . $this->variation . "\t" . $this->currentVariationName . "\t",FILE_APPEND);
            
         
            if(!empty($this->errorTestSteps)){//in case errors were found in the unit Testcases
            	array_push($errors, implode("\n",$this->errorTestSteps));
            }
            
            
            if(strpos(implode("\n",$errors),"View Error") === false){
                echo "\n\n\n>>>Testcase: " . $this->testCase . "(" . $this->url . ") - Passed.\n\n";
                $outputTextColor = "green";
                $errMsg = "Passed without any errors.";
                $this->countPassedTest++;
                $this->countCompletedTest++;
                
                file_put_contents("logger.txt","Passed\n", FILE_APPEND);
                
            }else{
                echo "\n\n\n>>>Testcase: " . $this->testCase . $this->testStatus . "(" . $this->url . ") \n\n" . implode("\n",$errors) . "\n\n";
                $outputTextColor = "#ff6543";
                $this->countCompletedTest++;
                
                //for test terminated with errors
                if(strpos($this->testStatus," - was terminated ") === 0){
                	$outputTextColor = "red";
                	$this->countTerminatedTest++;
                	$this->countCompletedTest--;
                	file_put_contents("logger.txt","Failed\n", FILE_APPEND);
                }else{
                	file_put_contents("logger.txt","CompletedWithErrors\n", FILE_APPEND);
                }   
                
                $errMsg = $this->testStatus . "with the following errors: \n\n" . implode("\n", $errors);
                
            }
			
            
            $this->errorForOutput[$this->testCase][] = "\n<p style='color:" . $outputTextColor . "'>". $errMsg . "</p>"; //concatenate errors for output html
              
        }
  
        $this->writeReport($this->errorForOutput);
     }

     
     public function initialiseTestCaseVariables(){
     	$this->var['1'] = '';
		$this->var['2'] = '';
		$this->var['3'] = '';
		$this->var['4'] = '';
		$this->var['5'] = '';
		$this->var['6'] = '';
		$this->var['7'] = '';
		$this->var['8'] = '';
		$this->var['9'] = '';
		$this->var['10'] = '';
		$this->var['11'] = '';
		$this->var['12'] = '';
		$this->var['13'] = '';
		$this->var['14'] = '';
		$this->var['15'] = '';
		$this->var['16'] = '';
		$this->var['17'] = '';
		$this->var['18'] = '';
		$this->var['19'] = '';
		$this->var['20'] = '';		
		$this->var['21'] = '';
		$this->var['22'] = '';
		$this->var['23'] = '';
		$this->var['24'] = '';
		$this->var['25'] = '';
		$this->var['26'] = '';
		$this->var['27'] = '';
		$this->var['28'] = '';
		$this->var['29'] = '';		
     }
	/**
	 * 
	 * @param string $theTime
	 * @return string
	 */
    public function shootThis($theTime = ''){
        $screenshot = $this->currentScreenshot();
        $screenShotPath = $this->path . $this->reportAltPath . 
        DIRECTORY_SEPARATOR . "Screenshots/" . $theTime . ".jpg";
        file_put_contents($screenShotPath, $screenshot);
        
        return "<||Screenshots/" . $theTime . ".jpg||>";
    }

    /**
     * @return bool|string
     */
    public function getDate(){
        return date("Y/m/d/h:i:sa");
    }

    /**
     * @return float
     */
    public function getVarInMicro(){
        return round(microtime(true) * 1000);
    }

    /**
     * Output Errors to report.html
     * @param $status
     */
    public function writeReport($status){

        $bodyElements = [];
        $body = "";

        
        
        $header = "<html>
                    <head>
        		         " . $this->getScript() . "
        		        
		                <script type=\"text/javascript\">			                        		         		
        		         	function showScreenshot(screenshotPath){
				                document.getElementById(\"showImage\").innerHTML = \"<div id='image'><img class='fixed' src='\" + screenshotPath + \"'/>\" + 
        		         		\"<button class='thebutton' onClick='hideIScreenshot()'>Close ScreenShot</button></div>\";        		         										
    						}
        		         		
        		         	function hideIScreenshot(){
                                document.getElementById('image').className += ' hide';
                            }
                        </script>

                        <style>
      	                    .fixed{
      			                position: fixed;
    			                top: 30;
				                right: 0;
    			                width: 50%;
    			                height: 100%;
                            }

                            .hide{
                                display: none;
                            }

                            .thebutton{
                                position:fixed;
                                top:0;
                                right:0;
                            }

                            #image{
                            }
        		         		
      	                </style>

        		        <center>
        		            <h1>Automation Test Results</h1><br>
        		            <h2>Testsuite: " . $this->testsuiteName . "</h2><br>
        		            Test performed on date " . $this->getDate() . "<br>
        		            Total no of test: " . $this->countTotalTest . " | Completed Tests: " . $this->countCompletedTest. " | Passed Test: " . $this->countPassedTest . " | Failed Tests: " . $this->countTerminatedTest . "        		            		
                        </center>
                    </head>";
        
        foreach ($status as $testcaseName=>$key){
            
        	$status[$testcaseName] = str_replace("\n", "<br>" , $status[$testcaseName]);
        	$status[$testcaseName] = str_replace("<||", "<button onclick=\"showScreenshot('", $status[$testcaseName]);
        	$status[$testcaseName] = str_replace("||>", "')\">Screenshot</button>", $status[$testcaseName]);
        	
        	$htmlId = strtolower(str_replace(" ", "", $testcaseName));
        	
	        if(is_array($status)) {
	            foreach ($status[$testcaseName] as $statusElement) {
	            	
	            	$testcaseColor = substr($statusElement, 4, strpos($statusElement, ">",4)-3);
	            	
	                $body = $body . "
	                		<p> 
	                			<h4>
	                				<div id=\"showImage\" style=\"float: right;\"></div>" . $testcaseColor . $testcaseName . "(" . $this->url . ") 
	                					<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#" . $htmlId. "result'>Details</button>
	                			</h4>
      									<div id='" . $htmlId . "result' class='collapse'><br>";			                		
	            }
	        }else{
	        }
	        
			$i=1;
	        foreach($this->testcaseStepsForOutput as $variationName => $variation){
	        	
	        	if(strpos($variationName,$testcaseName . "variation") === 0){
	        			        	
	        		$currentTestErrors = $statusElement;	        		
	        		
	        		if($statusElement !== "<br><p style='color:green'>Passed without any errors.</p>"){
	        			$currentTestErrors = substr($statusElement, strpos($statusElement,"<<<<<" . $variationName)+5+strlen($variationName));
	        		}
	        		
	        		if(strpos($currentTestErrors,"<<<<<") !== false){	        		
	        			$currentTestErrors = substr($currentTestErrors, 0, strpos($currentTestErrors,"<<<<<"));
	        		}
	        		
	        		if(strlen($currentTestErrors) > 5+strlen($variationName) && $currentTestErrors !== "<br><p style='color:green'>Passed without any errors.</p>"){
	        			$variationButtonColor = "red";
	        		}else{
	        			$variationButtonColor = "green";
	        		}
	        		
	        		
	        		if(strpos($variation[0],null) !== null){
	        			$variationNameButton = str_replace("-Variation|","",$variation[0]);
	        		}else{
	        			$variationNameButton = "Variation" . $i;
	        		}
	        		
	        		$currentTestErrors = str_replace("<div", "\"<div", $currentTestErrors);
	        		$currentTestErrors = str_replace("</div>", "</div>\"", $currentTestErrors);
	        		
		        	$body = $body . "<button type=\"button\" class=\"btn btn-info\" style=\"background-color:" . $variationButtonColor . "\"data-toggle=\"collapse\" data-target=\"#" . $variationName . "\"> " . $variationNameButton . "</button>
		                                    <div id=\"" . $variationName . "\" class=\"collapse\">" . $currentTestErrors . "<br>
		                            <table border='1'>";
	        		
		        	unset($currentTestErrors);
		        	
			        foreach($variation as $step){	
			            $step = str_replace("-Passed|","<tr bgcolor='green'><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$step);
			            $step = str_replace("-Failed|","<tr bgcolor='red'><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$step);
			            $step = str_replace("-Variation|","<tr bgcolor='#808080'><td style='margin-left: 0px'>",$step);
			            $body = $body . $step . "</td></tr>";
			        	
		        	}
		        	$i++;
		        	$body = $body . "</table></div><br>";
	        	}
	        }
	        $body = $body . "</table></div></div>";

        }

        	$body = $body . "<p><a href='Report.html'>Back</a>";
        	
        $htmlReport = $header;
        $htmlReport = $htmlReport . "<body> ". $body . "</body>";
        $htmlReport = $htmlReport . "</html>";


        file_put_contents($this->path . DIRECTORY_SEPARATOR . $this->reportAltPath . DIRECTORY_SEPARATOR . "Report_" . $this->sid . "_" . $testcaseName . "_variation_" . $this->variation. ".html", $htmlReport);

    }

    /**
     * Get string location based of presence of files
     * @return string
     */
    public function getScript(){
    	
    	if(gethostname() !== 'WIN-KMTS20D06A2'){
    		return " <script src=\"../../../../jsScripts/jquery-1.10.2.js\"></script>
        		        <link rel=\"stylesheet\" href=\"../../../../jsScripts/bootstrap.min.css\">
                        <script src=\"../../../../jsScripts/jquery.min.js\"></script>
                        <script src=\"../../../../jsScripts/bootstrap.min.js\"></script>
    	";
    	}else{
    		return "   <script src=\"jsScripts/jquery-1.10.2.js\"></script>
        		        <link rel=\"stylesheet\" href=\"jsScripts/bootstrap.min.css\">
                        <script src=\"jsScripts/jquery.min.js\"></script>
                        <script src=\"jsScripts/bootstrap.min.js\"></script>
    	";
    	}
    }
    
    /**
     * Get locator identifier
     * Get locator value
     *
     * @param $selector
     * @return WebElement
     */
    public function getWebElement($selector){

    	
        $selectorValue = substr($selector,1 + strpos($selector,"="));
        $selector = substr($selector,0,strpos($selector,"="));
        
        // /@ presence signifies attributes
        if(strpos($selectorValue,"/@") > 0){
            $selectorValue = substr($selectorValue,0,strpos($selectorValue,"/@"));
        }
        
        //For Dynamic Selectors (Passed as parameters)
        if (strpos($selectorValue,"var['") !== false){			
        	$selectorValue = $this->checkValue($selectorValue);        	
        }
        
        $this->extractedSelector = $selectorValue;
        
            switch ($selector) {

                case 'id':                      	
                    return $this->byId($selectorValue);
                    break;

                case 'css':
                    return $this->byCssSelector($selectorValue);
                    break;

                case 'xpath':                          
                    return $this->byXpath($selectorValue);
                    break;

                case "link":
                    return $this->byLinkText($selectorValue);
                    break;
                    
                case "file":                	                	
                	$selector = $this->fetchSelector($selectorValue);
                	$this->value = $this->fetchValue($selectorValue);
                	return $this->getWebElement($selector);
                	break;
                	
                case "data":
                	return $this->fetchData($selectorValue);
                	break;
            }
    }
    
    
    /**
     * return the selector from file
     * @param unknown $selector
     */
    public function fetchSelector($selector){    	    	
    	$selector = $this->currentSelectors->xpath("//" . $selector)[0];    
    	$selector = $selector = str_replace("'","\"",$selector);
    	return $selector;
    }
    
    
    /**
     * return the data from file
     * @param unknown $selector
     */
    public function fetchData($selector){
		
    	$selector = $this->dataSet->xpath("//" . $selector)[0];
    	$selector = (string)$selector;
    	
    	return $selector;
    }
    
    
    /**
     * @return default value of web element selected
     */
    public function fetchValue($selector){    	
    	
    	$value = $this->currentSelectors->xpath("//" . $selector . "/@default"); 		
    	if(isset($value[0])){
    		$value = (string)$value[0];
    	}else{
    		$value = "";
    	}
    	return $value;
    }
    
    
    /**
     * @return return attribute to be used
     */
    public function fetchAttribute($attribute){
    	$value = $this->currentSelectors->xpath("//" . $attribute . "/@attribute");
    	if(isset($value[0])){
    		$value = (string)$value[0];
    	}else{
    		$value = "";
    	}
    	return $value;
    }

    
    /**
     * Perform Calculations and return the value
     */
    public function calc($value){    	
    	    	
    	
    	$operator = preg_replace("/[0-9a-zA-Z.\]\[']/", "", $value);    	
    	$operator = preg_replace("@[/\\\]@", "", $operator);
    	
    	$firstValue = substr($value, 0, strpos($value,$operator));    	    
    	$firstValue = $this->checkValue($firstValue);    	
    	$secondValue = substr($value, strpos($value,$operator)+1);
     	$secondValue = $this->checkValue($secondValue);    	
     	
    	switch($operator){
    		case "+":
    			return $firstValue+$secondValue;
    			break;
    		case "-":
    			return $firstValue-$secondValue;
    			break;
    		case "/":
    			return $firstValue/$secondValue;
    			break;
    		case "*":
    			return $firstValue*$secondValue;
    			break;
    	}
    }
    
    
    /**
     * Reformat values used in fields
     *
     * @param $value
     * @return mixed
     */
    public function checkValue($value){

        //create random variable if value contains %
        if(strpos($value,"%%%") >= 0){
            $value = str_replace("%%%", round(microtime(true) * 1000), $value);
        }

        //retrieving information from arrays
        if (strpos($value, "var[") !== false && strpos($value, "var[var") === false){
            
        	$preValueText = $value;
        	$postValueText = $value;
        	
        	$noOfVariables = substr_count($value,"var");
        	
        	for($i=0 ; $i < $noOfVariables ; $i++){
        	        	
	            $preValueText = substr($value , 0, strpos($value,"[")-3);
	            $postValueText = substr($value, strpos($value,"]")+1);
	
	            $value = substr($value,strpos($value,"var[")+5);
	            $value = substr($value,0,strpos($value,"]")-1);
			
	            $value = $preValueText . $this->var[$value] . $postValueText;	
        	}
        }
        
        
        //for nested variables
        if (strpos($value, "var[var") !== false){
        	
        	$innerVar = substr($value, 0, strpos($value,"]]")-1);        	
        	$innerVar = substr($innerVar, strpos($innerVar,"var[var[")+9); // Nested Value Address
        	$outerVar = substr($value, 0, strpos($value,"]]")+2); 
        	$outerVar = substr($outerVar, strpos($outerVar,"var[var[")); //Get Var + nested Var        	
            $innerVarValue = $this->var[$innerVar]; // Get Nested Value            
            $value = str_replace($outerVar, "var['" . $innerVarValue . "']", $value); //Replacing Nested Var by its value                        
            $value = $this->checkValue($value); //Go into function recursively
        }
        
                
        if (strpos($value, "date[") !== false){        	        
        	$value = substr($value, strpos($value, "[")+2);
        	$value = substr($value, 0, strpos($value, "]")-1);
        	if(strpos($value, "strtotime") !== false){	        		
        		$valueFirst = substr($value, 0 , strpos($value,"'"));
        		$valueSecond = substr($value, strpos($value, "('")+2);        	
        		$valueSecond = substr($valueSecond, 0, strpos($valueSecond,"'"));
        		$value = date($valueFirst, strtotime($valueSecond));        		
        	}else {
        		$value = date($value);
        	}		
        }
        
        if(strpos($value,"data") !== false){
        	$value = substr($value, strpos($value, "[")+2);
        	$value = substr($value, 0, strpos($value, "]")-1);
        	$value = $this->fetchData($value);
        }
        
        return $value;
    }

    
    /**
     * Remove String white spaces and convert to lower cases.
     * @param String $input
     */
    public function formatInputs($input){
    	$input = strtolower($input);
    	$input = str_replace(' ', '', $input);
    	return $input;
    }
    
    
    /**
     * @return price in numbers, commas replaced by dot
     * @param string $price
     * @param string $decimal
     * 
     */
    public function formatPrices($price,$decimal=""){
    
    	//remove currency and spaces
    	$price = preg_replace("/[^0-9,.]/", "", $price);
    
    	if(isset($decimal)){
    		if($decimal === ","){
    			$price = str_replace(",",".", $price);
    		} else {
    			$price = str_replace(",","", $price);
    		}
    	}
    	return $price;
    }
    
    
    /**
     * return price in magento format
     * @param unknown $data is the price
     * @param unknown $separator e.g '.' or ','
     */
    public function reformatPrice($data,$separator){
    
    	//for 1d.p
    	if(strpos($data,".") !== false){
    		if(strlen(substr($data,strpos($data,".")+1)) == 1){
    			$data = $data . "0";
    		}
    	} else {// for 2d.p
    		$data = $data . ".00";
    	}
    	
    	
    	$separator = ",";
    	//getting unit of 3 digits separation
    	if($this->currencyFormat[0]== "d"){
    		$separator = ".";
    	}
    	
    	//separate digit by $selector
    	
    	$preDecimalDigits = substr($data,0,strrpos($data,"."));
    	$preDecimalDigitLength = strlen($preDecimalDigits);
    	
    	//separate only predigits longer than 3
    	if($preDecimalDigitLength > 3){
	    	$postDecimalDigits = substr($data,strrpos($data,"."));    	    
	    	$preDecimalDigitPart1 = substr($preDecimalDigits,0,$preDecimalDigitLength-3);
	    	$preDecimalDigitPart2 = substr($preDecimalDigits,$preDecimalDigitLength-3);    	
	    	$data = $preDecimalDigitPart1 . $separator . $preDecimalDigitPart2 . $postDecimalDigits;
    	}
    	return $data;
    }
    
    
    /**
     * 
     * @param string $price
     */
    public function priceFormat($price){
    	
    	$price = $this->reformatPrice($price, ".");      	
    	$newPrice = substr($this->currencyFormat, 0, strpos($this->currencyFormat,","));
    	$currency = substr($this->currencyFormat, strpos($this->currencyFormat,",")+1);    	
    	$newPrice = str_replace("d", "", $newPrice);
    	$newPrice = str_replace("s", " ", $newPrice);
    	$newPrice = str_replace("p", $price, $newPrice);
    	$newPrice = str_replace("c", $currency, $newPrice);    	
    	return $newPrice;
    }
    
    public function tearDown(){
    }

    /**
     *
     */
    public function clearMailinator($mail){
        $mail = strtolower(str_replace("@mailinator.com" , "" , $mail));
        $this->url("https://mailinator.com/inbox2.jsp?public_to=" . $mail . "#/#public_maildirdiv");

        while(1) {
            if ($this->byXPath("//div[3]/div/div/div/input")->displayed() !== null) {
                echo "\ni am in there\ncondition: " . $this->byXPath("//div[3]/div/div/div/input")->displayed();
                ob_flush();
                sleep(20);
                $this->byXPath("//div[3]/div/div/div/input")->click();
                echo "after the click\n";
                ob_flush();
                $this->byId("public_delete_button")->click();
                echo "after the delete\n";
                ob_flush();
            }
        }
    }


    /**
     * Assertion of expected value and actual value
     * @param $expectedMsg
     * @param $actualMsg
     * @return null|string
     */
    public function verifyMsg($expectedMsg, $actualMsg, $errMsg = ""){
        if ($expectedMsg === $actualMsg){
            return null;
        }
        return "\nError in Asserting that\nExpected value: " . $expectedMsg . "\nmatches\nActual: " . $actualMsg . "\nView Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
    }
    
    
    /**
     * Assertion of expected value and actual value
     * @param $expectedMsg
     * @param $actualMsg
     * @return null|string
     */
    public function verifyContainMsg($expectedMsg, $actualMsg, $errMsg = ""){
    	if (strpos($actualMsg,$expectedMsg) !== false){
    		return null;    		
    	}
    	return "\nError in Verifying that\nExpected value: " . $expectedMsg . "\nContains\nActual: " . $actualMsg . "\nView Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
    }


    /**
     * Assertion of true
     * @param $expectedValue
     * @param $actualValue
     * @param string $errMsg
     * @return null|string
     */
    public function verifyTrue($expectedValue, $actualValue, $errMsg = ""){
        if($expectedValue === $actualValue ){
            return null;
        }
        return "\nError in Asserting that Expected Value: " . $expectedValue . " Matches\nActual Value: " . $actualValue. "\nView Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
    }
    
    
    
    public function highlightElement($flag){
    	
    	if ($flag === 1){//highlight element when running test
    		$color = "thick solid #00FF00";
    	}else if ($flag === 0){//clear highlights
    		$color = "";
    	} else if ($flag === 2){//highlight element on error. Should be used carefully, if element is not there, this will fail
    		$color = "thick solid #FF0000";
    	}else{//break when step = js
    		break;
    	}
    	
    	$this->execute(array(
    			'script' => 'function getElementByXpath(path) {
        					return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        				}
        						getElementByXpath(\''. $this->extractedSelector . '\').style.border = "' . $color . '";
    	
        						',
    			'args'   => array()
    	));
    	
    }
    
    
    public function killInternet(){
    	if ($this->os === 'linux'){
    		$output = shell_exec('nmcli nm enable false');
    	}else if ($os === 'windows'){
			//     		
    	}else if ($os === 'windowsServer'){
    		//
    	}
    }
    
    public function enableInternet(){
    	if ($this->os === 'linux'){
    		$output = shell_exec('nmcli nm enable true');
    	}else if ($os === 'windows'){
    		//
    	}else if ($os === 'windowsServer'){
    		//
    	}
    }

    /**
     * Execute steps from html file
     *
     * @param string $action
     * @param string $selector
     * @param string $value
     * @return mixed
     */
    public function executeScripts($action, $selector, $value='', $step = ''){

        $errMsg = "";
        $failedAssertion = 0;

        //console output
        if($step == ""){//No Steps        	
        	echo "\n\t- " . $action . " - " . $selector . " | start ";
        }elseif($action == "step"){//For Step Functions
        	echo "\n\t- " . $this->checkValue($step) . " | start ";        	
        }else{//Normal steps
	        echo "\n\t- " . $this->checkValue($step) . " | start ";	        
        }
		ob_flush();
        
        
        //Check visibility of current web element
        $i=0;
        while(1 && strpos($selector,"=") !== false && strpos($selector,"data=") === false && $action !== "js") {
        	ob_flush();
        	try {
        		if ($i < 60) {
        			$currElement = $this->getWebElement($selector);
        			if (isset($currElement)) {
        				$currElement->displayed();        				
        				$this->highlightElement(1);        				
        				break;
        			}
        		}else{
        			echo "\nElement not Found for step after waiting for " . $i . "s: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";        			        			
        			break;
        		}
        	} catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
        		sleep(1);
        		$i++;
        	}
        	sleep(1);
        	$i++;
        	echo " " . $i;        	
        }
        
        

        //if value or var['value'] is blank, assign default one from Selectors.xml
        if($value === '' || $this->checkValue($value) === ''){
        	$value = $this->value;
        }
        
        //if value or var['value'] is "-", assign Blank
        if($value === "--" || $this->checkValue($value) === '--'){
        	$value = "";
        }


        try {
            switch ($action) {
                case "click":
                    if(isset($currElement)) {                    	                    	
                    	$currElement->click();                    	
                    }
                    break;

                case "clickandwait":
                    if(isset($currElement)) {
                        $currElement->click();
                    }
                    break;

                case "open":
                	
                    if($selector === "") { //if blank, open website's url
                        $this->url($this->http . "://" . $this->htaccess . $this->url);
                    }elseif(strpos($selector,"http") === 0 || strpos($selector,"var") !== false){ //if http present, open the url present in selector
                        $this->url($this->checkValue($selector));
                    }elseif(strpos($selector, "adminurl") !== false){ //if adminurl present, open admin
                    	$this->url($this->http . "://" . $this->htaccess . $this->adminurl);
                    }else{
                        $this->url("http://" . $this->htaccess . $this->url . "/" . $selector); //if none of above, concatenate url with selector
                    }
                    
                    $this->prepareSession()->currentWindow()->maximize();                    
                    break;

                case "curl":
                	//initialise Curl
                	$ch = curl_init();
                	
                	//$url to send in request
                	curl_setopt($ch, CURLOPT_URL, "http://www.google.mu");
                	
                	//Return Transfer
                	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                	
                	//Do not include header
                	curl_setopt($ch, CURLOPT_HEADER, 0);
                	
                	//Execute Request
                	$output = curl_exec($ch);
                	
                	//close curl
                	curl_close($ch);
                	
                	file_put_contents("/home/yassar/Desktop/curlTest2.html",$output);
                	break;
                    
                case "selectiframe":
                	$this->frame($currElement);                	
                	break;
                	
                case "step":                	
                	
                	$unitTestCaseName = $selector;
                	$i = 0;
                	$valueAsArray = explode(",", $value);
                	//assign args as variables
                	foreach($valueAsArray as $arg){
                		//Ensure that variables also can be passed
                		$arg = $this->checkValue($arg);
                		
                		switch($i){//can accomodate max of 30 arguments 
                			case 0:
                				$this->var['1'] = $arg;
                				break;
                			case 1:
                				$this->var['2'] = $arg;
                				break;
                			case 2:
                				$this->var['3'] = $arg;
                				break;
                			case 3: 
                				$this->var['4'] = $arg;
                				break;
                			case 4:
                				$this->var['5'] = $arg;
                				break;
                			case 5: 
                				$this->var['6'] = $arg;
                				break;
                			case 6:
                				$this->var['7'] = $arg;
                				break;
                			case 7:
                				$this->var['8'] = $arg;
                				break;
                			case 8:
                				$this->var['9'] = $arg;
                				break;
                			case 9:
                				$this->var['10'] = $arg;
                				break;
                			case 10: 
                				$this->var['11'] = $arg;
                				break;
                			case 11: 
                				$this->var['12'] = $arg;
                				break;
                			case 12: 
                				$this->var['13'] = $arg;
                				break;
                			case 13:
                				$this->var['14'] = $arg;
                				break;
                			case 14:
                				$this->var['15'] = $arg;
                				break;
                			case 15:
                				$this->var['16'] = $arg;
                				break;
                			case 16: 
                				$this->var['17'] = $arg;
                				break;
                			case 17:
                				$this->var['18'] = $arg;
                				break;
                			case 18: 
                				$this->var['19'] = $arg;
                				break;
                			case 19:
                				$this->var['20'] = $arg;
                				break;
                			case 20:
                				$this->var['21'] = $arg;
                				break;
                			case 21:
                				$this->var['22'] = $arg;
                				break;
                			case 22:
                				$this->var['23'] = $arg;
                				break;
                			case 23:
                				$this->var['24'] = $arg;
                				break;
                			case 24:
                				$this->var['25'] = $arg;
                				break;
                			case 25:
                				$this->var['26'] = $arg;
                				break;
                			case 26:
                				$this->var['27'] = $arg;
                				break;
                			case 27:
                				$this->var['28'] = $arg;
                				break;
                			case 28:
                				$this->var['29'] = $arg;
                				break;
                			case 30:
                				$this->var['30'] = $arg;
                				break;
                		}
 
                		$i++;
                	}
                	                	
                	$testcase = $this->path . $this->unitTestCasesPath . $unitTestCaseName . ".html";
                	$currentErrors = $this->decomposeHtml2($testcase);
                	
                	foreach($currentErrors as $error){
                		$this->errorTestSteps[] = $error;
                	}
                	
                	$this->initialiseTestCaseVariables();
                	break;
                    
                	
                case "type":
                    if(isset($currElement)) {                    	
                        $this->moveto($currElement);
                        $currElement->clear();
                        $currElement->value($this->checkValue($value));                         
                    }
                    break;
                    
                case "clear":
                	$currElement->clear();
                	break;

                case "select":
                    if(isset($currElement)) {
                        $currElement->value($this->checkValue($value));
                    }
                    break;
                    
                case "cookie":
                
                	break;

                case "asserttext":
                    if(isset($currElement)) {
                        $msg = $currElement->text();
                        $failedAssertion = 1;
                        $this->assertEquals($this->checkValue($value), $msg);
                        $failedAssertion = 0;
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                        //fail the test when element not found
                        $this->assertTrue(false,$this->checkValue($value) . " Not Found");
                    }
                    break;

                case "assertcontainattribute":
                    if(isset($currElement)){                    	
                    	$selectorValue = substr($selector,1 + strpos($selector,"="));//breaking down the 'file' from the xpath               	
                    	$selector = $this->fetchSelector($this->checkValue($selectorValue));            //fetch the selector        	
//                     	$attribute = substr($selector,strpos($selector,"/@")+2);	 
						$attribute = $this->fetchAttribute($this->checkValue($selectorValue));			 //fetch the attribute
                    	
//                     	echo "\nselectorValue: " . $selectorValue . "\nselector: " . $selector . "\nattribute: " . $attribute . "\n";
                    	
                        $msg = $currElement->attribute($attribute);                        
                        $failedAssertion = 1;
                        $errMsg = $this->assertEquals($this->checkValue($value), $msg);
                        $failedAssertion = 0;
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    }
                    break;


                case "assertvalue":
                    if(isset($currElement)) {
                        $msg = $currElement->text();
                        $failedAssertion = 1;
                        $this->assertEquals($this->checkValue($value), $msg);
                        $failedAssertion = 0;
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    }
                    break;

                case "asserttitle":
                    $theTitle = $this->title();

                    if($value === ""){
                        $value = "Cannot Assert Title to be " . $selector . "\n";
                    }
                    $failedAssertion = 1;
                    $this->assertEquals($selector,$this->title(),$value);
                    $failedAssertion = 0;
                    break;

               case "assertcontaintitle":
                    	$theTitle = $this->title();
                    
                    	if($value === ""){
                    		$value = "Cannot Assert Title to be " . $selector . "\n";
                    	}
                        $failedAssertion = 1;
                    	$this->assertContains($selector,$this->title(),$value);
                        $failedAssertion = 0;
                    	break;
                    
                    
                case "verifytext":
                    if(isset($currElement)){
                        $msg = $currElement->text();
                        $errMsg = $this->verifyMsg($this->checkValue($value),$msg);
                        if ($errMsg !== null){
                            $failedAssertion = 1;
                        }
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    }
                    break;

                   
                case "verifyprice":
                	if(isset($currElement)){
                		$negativeFlag = 0;
                		$msg = $currElement->text();
                		//for negative values
                		if(strpos($value, "-") !== false){
                			$value = str_replace("-", "", $value);
                			$negativeFlag = 1;
                		}
                		
                		$price = $this->checkValue($value);
                		$price = $this->priceFormat($price);
                		//for negative values
                		if($negativeFlag == 1){
                			$price = "-" . $price;                			
                		}
                		
                		$errMsg = $this->verifyMsg($price,$msg);
                		if ($errMsg !== null){
                			$failedAssertion = 1;
                		}
                	}else{
                		$errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                		echo $errMsg;
                		$failedAssertion = 1;
                		$errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                	}
                	break;
                	
                	
                	case "verifycontainprice":
                		if(isset($currElement)){
                			$negativeFlag = 0;
                			$msg = $currElement->text();

                			//for negative values
                			if(strpos($value, "-") !== false){
                				$value = str_replace("-", "", $value);
                				$negativeFlag = 1;
                			}
                	
                			$price = $this->checkValue($value);
                			$price = $this->priceFormat($price);
                			
                			//for negative values
                			if($negativeFlag == 1){
                				$price = "-" . $price;
                			}
                	
                			$errMsg = $this->verifyContainMsg($price,$msg);
                			if ($errMsg !== null){
                				$failedAssertion = 1;
                			}
                		}else{
                			$errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                			echo $errMsg;
                			$failedAssertion = 1;
                			$errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                		}
                		break;
                    
                case "verifycontaintext":
                 	if(isset($currElement)){
                   		$msg = $currElement->text();
                   		$errMsg = $this->verifyContainMsg($this->checkValue($value),$msg);
                   		if ($errMsg !== null){
                   			$failedAssertion = 1;
                   		}
                   	}else{
                   		$errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                   		echo $errMsg;
                   		$failedAssertion = 1;
                   		$errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                   	}
                   	break;
                    
                case "reset":
                	$this->prepareSession()->cookie()->clear();
                	break;

                case "assertcontaintext":
                    if(isset($currElement)) {
                        $msg = $currElement->text();
                        $failedAssertion = 1;
                        $this->assertContains($this->checkValue($value), $msg);
                        $failedAssertion = 0;
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    }
                        break;

                case "asserttextultimate":
                    if(isset($currElement)) {
                        $msg = $currElement->text();
                        $failedAssertion = 1;
                        $this->assertEquals($this->checkValue($value), $msg);
                        $failedAssertion = 0;
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    }
                    break;

                case "assertcontaintitleultimate":
                	$theTitle = $this->title();
                	
                	if($value === ""){
                		$value = "Cannot Assert Title to be " . $selector . "\n";
                	}
                	$failedAssertion = 1;
                	try{
                		$this->assertContains($selector,$this->title(),$value);                		
                		$failedAssertion = 0;
                	}catch(Exception $e){                		                		
                		$errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";                		
                		echo $errMsg;
                		$errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                		$this->testcaseStepsForOutput[$this->testCase . 'variation' . $this->currentVariationIndex][] = "-Failed|" . $step . " [ " . $action . "| " . $selector . "| " . $this->checkValue($value) . "] ";                		
                		$this->errorForOutput[$this->testCase][] = "\n<p style='color:red'>". $errMsg . "</p>";                		
                		$this->writeReport($this->errorForOutput);                		
                		die();
                	}
                	                	 
                	break;
                   
                case "assertelementpresent":
                    if(isset($currElement)){
                        $failedAssertion = 1;
                        $this->assertTrue(true,"Element Found");
                        $failedAssertion = 0;
                    }else{
                        $errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                        $this->assertTrue(false,"Message Not Found");
                    }
                    break;

                case "verifyelementpresent":
                    if(isset($currElement)){
                        $errMsg = $this->verifyTrue(true,true);
                        if($errMsg !== null){
                            $failedAssertion = 1;
                        }
                    }else{
                        $errMsg = $this->verifyTrue(true,false);
                        echo $errMsg;
                        $failedAssertion = 1;
                        $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    }                    
                    break;

                case "waitfortext":
                    $i = 0;
                    while(1){
                        try{
                            if($i < 10) {
                                $currElement = $this->getWebElement($selector);
                                if(isset($currElement)) {
                                    if ($currElement->text() === $this->checkValue($value)) {
                                        break;
                                    }
                                }
                            }else{
                                $errMsg = "\nElement not Found for step after waiting for " . $i ."s: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                                echo $errMsg;
                                $failedAssertion = 1;
                                $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                                break;
                            }
                        }catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e){
                        	sleep(1);
                        	$i++;
                        }
                        sleep(1);
                        $i++;
                    }
                    break;

                case "waitfortextelsequit":
                    $i = 0;

                    while(1){
                        try{
                            if($i < 10) {
                                $currElement = $this->getWebElement($selector);
                                if(isset($currElement)) {
                                    if ($currElement->text() === $this->checkValue($value)) {
                                        break;
                                    }
                                }
                            }else{
                                $errMsg = "\nFatal Error! Element not Found for step: " . $action . "|" . $selector . "|" . $value ."\n" . $step . "\n";
                                echo $errMsg;
                                $failedAssertion = 1;
                                $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                                $this->assertTrue(false, "Fatal Error: Element not found\n");
                            }
                        }catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e){
                        	sleep(1);
                        	$i++;
                        }
                        sleep(1);
                        $i++;
                    }
                    break;

                case "waitfortitle":
                    $i = 0;
                    while(1){
                        try{
                            if($i < 10) {
                                $currElement = $this->title();
                                if(isset($currElement)) {
                                    if ($currElement === $this->checkValue($value)) {
                                        break;
                                    }
                                }
                            }else{
                                $errMsg = "\nElement not Found for step after waiting for " . $i . "s: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                                echo $errMsg;
                                $failedAssertion = 1;
                                $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                                break;
                            }
                        }catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e){
                        	sleep(1);
                        	$i++;
                        }
                        sleep(1);
                        $i++;
                    }
                    break;
                    break;

                case "waitfortextelsecontinue":
                    $i = 0;
                    while(1){
                        try{
                            if($i < 20) {
                                $currElement = $this->getWebElement($selector);
                                if(isset($currElement)) {
                                    if ($currElement->text() === $this->checkValue($value)) {
                                        break;
                                    }
                                }
                            }else{
                                $errMsg = "\nElement not Found for step after waiting for " . $i . "s: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                                echo $errMsg;
                                $failedAssertion = 1;
                                $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                                break;
                            }
                        }catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e){
                        	sleep(1);
                        	$i++;
                        }
                        sleep(1);
                        $i++;
                    }
                    break;


                case "waitfor":
                    sleep($selector);
                    break;

                case "waitforelementpresent":
                    $i=0;
                    while(1) {
                        try {
                            if ($i < 20) {
                                $currElement = $this->getWebElement($selector);
                                if (isset($currElement)) {
                                    $currElement->displayed();
                                    break;
                                }
                            }else{
                                $errMsg = "\nElement not Found for step after waiting for " . $i . "s: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                                echo $errMsg;
                                $failedAssertion = 1;
                                $errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                                break;
                            }
                        } catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
                        	sleep(1);
                        	$i++;
                        }
                        sleep(1);
                        $i++;
                        echo " " . $i;
                    }
                    break;

                case "waitforelementabsent":

                    while(1){
                        try{
                            $currElement = $this->getWebElement($selector);
                            if(isset($currElement)){
                                $currElement->displayed();
                            }
                        }catch(PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e){
                            break;
                        }
                    }
                    break;

                case "closepopup":
                    $currElement = $this->getWebElement($selector);
                    $this->getWebElement("class=._show .modal-inner-wrap button.action-close")->click();
                    break;

                case "variable":            
                	//getting info from data set and assigning to variable
                	//format of use: variable|data='entity/entityname'|variablename
                	if(strpos($selector,"=") !== false){
                		$variableName = $value;
                		$data = $this->getWebElement($selector);                		
                		$this->var[$variableName] = $this->checkValue($data);
                	}else{//normal variable assignment
                		$this->var[$selector] = $this->checkValue($value);
                	}                	
                    break;
                    
                case "variableprice":                	
                	$value = $this->formatPrices($value,",");
                	$this->var[$selector] = $this->checkValue($value);                	
                	break;
                    
                case "storetext":
                	$currElement = $this->getWebElement($selector);                	
                	$this->var[$value] = $currElement->text();
                	break;
                	
                case "stepstoretext":
                	$currElement = $this->getWebElement($selector);
                	$this->var[$this->checkvalue($value)] = $currElement->text();                	
                	break;
                	

                case "modifystring":
                	
                	if (strpos($value,",") !== false){
                		$values = explode(",", $value);                	
                		$values[0] = $this->checkValue($values[0]);
                		$values[1] = $this->checkValue($values[1]);
                		
                		$substring = substr($this->checkValue($selector), $values[0],$values[1]);                		
                	}else{
                		$substring = substr($this->checkValue($selector), $value);
                	}
                	
                	
                	$selector = substr($selector, 5); //removes var['
                	$selector = substr($selector, 0,strlen($selector)-2); //remove ']
                	$this->var[$selector] = $substring;                	
                	break;
                
                case "storeprice":                	
                	$currElement = $this->getWebElement($selector);
                	$currElement = $currElement->text();                	
					$currElement = $this->formatPrices($currElement,",");
					$this->var[$value] = $currElement;									
                	break;

                case "stepstoreprice":
                	$currElement = $this->getWebElement($selector);
                	$currElement = $currElement->text();
                	$currElement = $this->formatPrices($currElement,",");
                	$this->var[$this->checkvalue($value)] = $currElement;
                	break;
                		 
                	
                case "storeattribute":
                	      
                	if(isset($currElement)){
                		$attribute = substr($selector,strpos($selector,"/@")+2);
                		$attribute = $currElement->attribute($attribute);
                		$this->var[$value] = $attribute;
                	}else{
                		$errMsg = "\nElement not Found for step: " . $action . "|" . $selector . "|" . $this->checkValue($value) ."\n" . $step . "\n";
                		echo $errMsg;
                		$failedAssertion = 1;
                		$errMsg = $errMsg . "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n";
                	}                	                	
                	break;
                	
                case "calc":                	
                	$currElement = $this->calc($selector);                	
                	//'If' equals true if a variable is passed to be assigned
                	if(strpos($value,"var[") !== false){
                		$value = $this->checkValue($value);
                	}                	
                	$this->var[$value] = $currElement;                	
                	break;
                	
                case "reformatprice":                	
                	$this->var[$selector] = $this->reformatPrice($this->var[$selector], $value);                	
                	break;
                	
                case "echo":
                    $this->checkValue($selector);
                    break;

                case "message":
                    break;

                case "switchtonewwindow":                	
                    $allWindowHandles = $this->windowHandles();                    
                    $this->window($allWindowHandles[1]);                    
                    break;

                case "function":
                    call_user_func(array($this, $selector),$value);
                    break;
                    
                case "control":
                	$breaker = "";
                	while($breaker === ""){
                		                		
                		$ctrlAction = readline("Enter actions: ");                	
                		$ctrlAction = explode("|", $ctrlAction);
                		
                		if($ctrlAction[0] === "exit"){
                			break;
                		}
                		
                		$value = '';
                		if(isset($ctrlAction[2])){
                			$value = $this->checkValue($ctrlAction[2]);	
                		}
                		
                		echo $this->executeScripts($this->formatInputs($ctrlAction[0]), $this->formatInputs($ctrlAction[1]), $value, '');
                		echo $errMsg;
                		ob_flush();
                	}
                	
                	break;
                	
                case "assertalert":
                	$i=0;
                	try{
	                	while($this->alertText() == "" || $i<20){
                			$this->assertEquals($selector, $this->alertText());
                			$i++;
	                	}
                	}catch(Exception $e){
                		
                	}
                	break;
                	
                case "js":
                	$this->execute(array(
   					 'script' => $selector,
    				 'args'   => array()
						));
                	break;

                	
                case "killinternet":
                	$this->killInternet();
                	break;
                	
                	
                case "enableinternet";
                	$output = shell_exec('nmcli nm enable true');
                break;
                	
                
                case "allstepscompleted":
                    $this->testStatus = " - was completed ";                                        
//                     $errMsg = "Success: " . $this->shootThis($this->getVarInMicro()) . "\n";
                    break;

                default:
                    $errMsg = " - Failed - Action does not exist. Please verify step: " . $action . "|" . $selector . "|" . $this->checkValue($value) . "\n" . $step . "\n";
                    echo $errMsg;
                    $failedAssertion = 1;
                    break;
            }
            
            //remove highlight from current element
            try{
            $this->highlightElement(0);
            }catch(PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e){}
            
        }
	catch (Exception $e){

            
        $this->error++;

        echo " - end";
        ob_flush();

        $this->testcaseStepsForOutput[$this->testCase . 'variation' . $this->currentVariationIndex][] = "-Failed|" . $step . " [ " . $action . "| " . $selector . "| " . $this->checkValue($value) . "] ";
        return "View Error: " . $this->shootThis($this->getVarInMicro()) . "\n" . $e;
        }

        echo " - end";
        ob_flush();

        if($failedAssertion == 0 ){
            $this->testcaseStepsForOutput[$this->testCase . 'variation' . $this->currentVariationIndex][] = "-Passed|" . $step . " [ " . $action . "| " . $selector . "| " . $this->checkValue($value) . "] ";
        }else {
            $this->testcaseStepsForOutput[$this->testCase . 'variation' . $this->currentVariationIndex][] = "-Failed|" . $step . " [ " . $action . "| " . $selector . "| " . $this->checkValue($value) . "] ";
        }
        
        return $errMsg;
    }



}


