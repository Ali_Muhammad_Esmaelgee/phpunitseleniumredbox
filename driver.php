<?php


class driver{

	/**
	 * session of current test
	 * String
	 */
	protected $sid;
	
	/**
	 * testsuite name
	 * @var string $testsuiteName
	 */
	protected $testsuiteName;
	

	/**
	 * Name of the project
	 * @var string 
	 */
	protected $projectname;
	
	/**
	 * This is the browser used to run test
	 * @var string 
	 */
	protected $browser;
	
	/**
	 * http or https.
	 * @var string 
	 */
	protected $http;
	
	/**
	 * port number for test to run on
	 * @var string
	 */
	protected $port;
	
	/**
	 * $url of website to be tested
	 * @var String 
	 */
	protected $url;
	
	/**
	 * Url of admin
	 * @var string
	 */
	protected $adminurl;
	
	/**
	 * htaccess of website
	 * @var string 
	 */
	protected $htaccess;
	
	/**
	 * path of the testcase
	 * @var string
	 */
	protected $path;
	
	/**
	 * currency and format of the current website
	 * @var string 
	 */
	protected $currencyFormat;
	
	/**
	 * Alternative path for storing screenshots and report
	 * @var string
	 */
	protected $reportAltPath;
	
	
	/**
	 * Build Test
	 */
	public function __construct($inlineTestcase){
		
		file_put_contents("logger.txt","");
		$this->createSessionId();		
		$testcases = $this->readTestSuite();
		
		if($inlineTestcase == null){
			$this->runTestSuite($testcases);
		}else{
			$this->runInlineTestcase($inlineTestcase);
		}
		$this->createReport();
	}
	
	
	public function createReport(){
		$logs = file_get_contents("logger.txt");
		$templogger = explode("\n", $logs);
		array_pop($templogger);
		
		$header = "<html>
                    <head>
		                <script type=\"text/javascript\">
        		         	function showScreenshot(screenshotPath){
				                document.getElementById(\"showImage\").innerHTML = \"<div id='image'><img class='fixed' src='\" + screenshotPath + \"'/>\" +
        		         		\"<button class='thebutton' onClick='hideIScreenshot()'>Close ScreenShot</button></div>\";
    						}
        		 
        		         	function hideIScreenshot(){
                                document.getElementById('image').className += ' hide';
                            }
                        </script>
		
                        <style>
      	                    .fixed{
      			                position: fixed;
    			                top: 30;
				                right: 0;
    			                width: 50%;
    			                height: 100%;
                            }
		
                            .hide{
                                display: none;
                            }
		
                            .thebutton{
                                position:fixed;
                                top:0;
                                right:0;
                            }
		
                            #image{
                            }
        		 
      	                </style>
		
        		        <center>
        		            <h1>Automation Test Results</h1><br>
        		            <h2>Testsuite: " . $this->testsuiteName . "</h2><br>
        		            Test performed on date " . date("Y/m/d/h:i:sa") . "<br>
         		            Total no of test: countTotalTest  | Completed Tests: countCompletedTest | Passed Test: countPassedTest  | Failed Tests: countTerminatedTest 
                        </center>
                    </head>";
        		   
		
		$body = "<body>
        		    	<table border='1'>";
		
		foreach($templogger as $log){
			$currentLog = explode("\t", $log);			
						
			if($currentLog[2] === "Passed"){
				$color = "style='color:green'";	
			}else if($currentLog[2] === "Failed"){
				$color = "style='color:red'";
			}else if($currentLog[2] === "CompletedWithErrors"){
				$color = "style='color:orange'";
			}
			
			$testcaseName = (string)$currentLog[0];
// 			echo "testcasename: " . $testcaseName . "\n"; 
			$testcaseName = substr($testcaseName,0,strpos($testcaseName,"variation"));
// 			echo "testcasename after substrin: " . $testcaseName  ."\n\n";
			
			
			$body = $body . 
			"<tr>
				<td>" . $testcaseName . " - " . $currentLog[1] . "</td>
				<td><a href=\"Report_" . $this->sid . "_" . $currentLog[0] . ".html\">View Details</td>
				<td " . $color . ">" . $currentLog[2] . "</td>
			</tr>";
		}
		
		
		$body = $body . "</table>
				</body>";
		
		
		if(!empty($this->reportAltPath)){			
			$reportPath = $this->path . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . $this->reportAltPath . DIRECTORY_SEPARATOR; // this line is for direct use of the reportAltPath in class
		}else{
			$reportPath = $this->path . DIRECTORY_SEPARATOR . $this->reportAltPath . DIRECTORY_SEPARATOR;
		}
		
		file_put_contents($reportPath . "Report.html", $header . $body . "</html>");
		
	}
	
	
	public function createSessionId(){		
		$this->sid = round(microtime(true) * 1000);
	}
	
	
	public function writeCurrentTest($testcase, $variation){
		
		$filecontent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<currentTestPlan>\n";
		$filecontent = $filecontent . "\t<SID>" . $this->sid . "</SID>\n";
		$filecontent = $filecontent . "\t<testsuite> " . $this->testsuiteName. " </testsuite>\n";
		$filecontent = $filecontent . "\t<projectname>" . $this->projectname . "</projectname>\n";
		$filecontent = $filecontent . "\t<path>" . $this->path . "</path>\n";
		$filecontent = $filecontent . "\t<http>" . $this->http . "</http>\n";
		$filecontent = $filecontent . "\t<port>" . $this->port . "</port>\n";
		$filecontent = $filecontent . "\t<browser>" . $this->browser . "</browser>\n";
		$filecontent = $filecontent . "\t<url>" . $this->url . "</url>\n";
		$filecontent = $filecontent . "\t<adminurl>" . $this->adminurl . "</adminurl>\n";
		$filecontent = $filecontent . "\t<htaccess>" . $this->htaccess . "</htaccess>\n";
		$filecontent = $filecontent . "\t<currencyFormat>". $this->currencyFormat . "</currencyFormat>\n";
		$filecontent = $filecontent . "\t<testcase>" . $testcase . "</testcase>\n";
		$filecontent = $filecontent . "\t<variation>" . $variation . "</variation>\n";
		
		if(!empty($this->reportAltPath)){
			$filecontent = $filecontent . "\t<reportAltPath>" . $this->reportAltPath . "</reportAltPath>\n";
		}
		
		$filecontent = $filecontent . "\t</currentTestPlan>\n";
		
		file_put_contents("currentTest.xml", $filecontent);		
	}
	
	
	public function readTestSuite(){
		
		        $testsuite = simplexml_load_file("TestSuite.xml") or die("Cannot open TestSuite.xml");
		        $this->testsuiteName = (string)$testsuite->xpath("//testsuite/@name")[0];
		        $testcases = $testsuite->xpath("//testcase");
		        $this->projectname = (string)$testsuite->xpath("//project/@name")[0];
		        $this->http = (string)$testsuite->xpath("//project/@http")[0]; 
		        $this->port = (string)$testsuite->xpath("//project/@port")[0];
		        $this->path = (string)$testsuite->xpath("//project/@path")[0];
		        $this->browser = (string)$testsuite->xpath("//project/@browser")[0];
		        $this->url = (string)$testsuite->xpath("//project/@url")[0];
		        $this->adminurl = (string)$testsuite->xpath("//project/@adminurl")[0];
		        $this->htaccess = (string)$testsuite->xpath("//project/@htaccess")[0];
				$this->reportAltPath = (string)$testsuite->xpath("//project/@reportAltPath")[0];
				$this->currencyFormat = (string)$testsuite->xpath("//project/@currencyFormat")[0];				
				
// 				echo "\n\ntestsuitename: " . $testsuiteName . "\n";			
// 				echo "port: " . $port . "\n";
// 				echo "path: " . $path . "\n";
// 				echo "browser: " . $browser . "\n";
// 				echo "url: " . $url . "\n";
// 				echo "htaccess: " . $htaccess . "\n";
// 				echo "reportAltPath: " . $reportAltFolder . "\n";
// 				echo "currencyFormat: " . $currencyFormat . "\n";

				return $testcases;				
	}
	
	
	
	/**
	 * loops through all the testcases, creates currentTest.xml and run phpunit
	 * @param all testcases found in testsuite
	 */
	public function runTestSuite($testcases){		
		
		foreach ($testcases as $testcase){
	
			$variationIndex = array();
			
			$variation = (string)$testcase["variation"];
			
			if($variation === "*"){
				$variationIndex = $this->getVariationFromTestCase($testcase);
			}else{
				$variationIndex = (string)$testcase["variation"];
			}		
			
			$variationIndex = explode(",", $variationIndex);
			
			foreach($variationIndex as $currentVariation){
				$this->writeCurrentTest($testcase,$currentVariation);				
				system("phpunit");
			}
			
		}
	}
	
	
	public function runInlineTestcase($inlineTestcase){
		$variation = substr($inlineTestcase, strlen($inlineTestcase)-1 , strlen($inlineTestcase));
		$inlineTestcase = substr($inlineTestcase, 0, strlen($inlineTestcase)-1);
		
		if($variation === "*"){
			$variationIndex = $this->getVariationFromTestCase($inlineTestcase);
		}else{
			$variationIndex = $variation;
		}
		
		$variationIndex = explode(",", $variationIndex);
			
		foreach($variationIndex as $currentVariation){
			$this->writeCurrentTest($inlineTestcase,$currentVariation);
			system("phpunit");
		}
		
	}
	
	
	/**
	 * 
	 * @param String current testcase
	 * @return string variations list from current testcase (numbers) 
	 */
	public function getVariationFromTestCase($testcase){
		
		$file = $this->path . DIRECTORY_SEPARATOR . $testcase . ".html";		
		$html = file_get_contents($file);       //reads file into $html
		$doc = new DOMDocument();               //Initialise Dom Doc
		$doc->loadHTML($html);                  //load variable as html
		$doc = simplexml_import_dom($doc);		
		
		$i=2;
		$variation = "";
		
		
		//iterates through testcase 
		//breaks if it find EndTest
		//else, check 1st column for variation no
		//assign 'variation' for not null
		
		while (1){
			$step = $doc->xpath("/html/body/table/tbody/tr[ " . $i . "]/td[2]/font");
			$step = (string)$step[0];
						
			if(strpos($step,"EndTest") !== false){			
				break;
			}else {
				$tempVariation = $doc->xpath("/html/body/table/tbody/tr[" . $i . "]/td[1]/font");
				$tempVariation = (string)$tempVariation[0];				
				if($tempVariation !== ""){
					$variation = $tempVariation;										
				}								
			}
			$i++;			
		}
		
		//extracting last variation no  
		$variation = str_replace($testcase, "", $variation);				
		$variation = substr($variation, 0, strpos($variation,"_"));		
		$variation = substr($variation, 13);		
		$variations = "";
		
		//concatenating variation in 1 variable
		for ($i=1 ; $i <= $variation ; $i++){
			$variations = $variations . $i . ",";
		}
		
		return substr($variations, 0,strlen($variations)-1);
	}
	
	
}

$testcase = null;
if(isset($argv[1])){
	$testcase = $argv[1];
}

new driver($testcase);



