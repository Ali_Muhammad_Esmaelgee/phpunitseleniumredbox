#!/bin/bash

#Get last build
currentBuildNo=$(curl -s http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/lastBuild/ | grep Automation)
currentBuildNo=$(echo ${currentBuildNo:37:3})

#Build Project
curl -sX POST http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/buildWithParameters?url=screwfix.de.jt.redboxcloud.com

currentProjectStatus=$(curl -sX POST http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/ | grep "In progress > Console Output")
i=0;

while [ -z "$currentProjectStatus" ]
do
	currentProjectStatus=$(curl -sX POST http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/ | grep "In progress &gt; Console Output")

	#break if script takes more than 2mins to launch
	if [ $i -gt 120 ]
	then
	break
	fi

	i=$(($i+1))
	sleep 1
done

echo "Build was started successfully"

while [ ! -z "$currentProjectStatus" ]
do
currentProjectStatus=$(curl -sX POST http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/ | grep "In progress &gt; Console Output")
done

echo "Build is complete"

#Calculate new Build no
newBuildNo=$(($currentBuildNo+1))

#initialise new console log
currentErrorLog=$(curl -s http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/lastBuild/ | grep "Automation - Screwfix-De #$newBuildNo Console")

#continue while current build log is not set
while [ -z "$currentErrorLog" ]
do 
currentErrorLog=$(curl -sX POST http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/lastBuild/console | grep "Automation - Screwfix-De #$newBuildNo Console")
done

curl http://JenkinsTrigger:95c533d52425bcc3d2d415bd1869b5f0@52.48.62.5:8080/job/Automation%20-%20Screwfix-De/lastBuild/console
