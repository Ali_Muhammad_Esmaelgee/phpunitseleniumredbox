<?php
$xmlFile = simplexml_load_file('TestSuite.xml') or die("i cannot open file");

$oldUrl = $xmlFile->xpath("//project/@url");
$oldPort = $xmlFile->xpath("//project/@port");
$oldBrowser = $xmlFile->xpath("//project/@browser");
$oldAdminUrl= $xmlFile->xpath("//project/@adminurl");
$oldHttp = $xmlFile->xpath("//project/@http");

$xmlString = $xmlFile->asXml();

$url = $oldUrl[0];
$port = $oldPort[0];
$browser = $oldBrowser[0];
$adminurl = $oldAdminUrl[0];
$http = $oldHttp[0];

//$url = $argv[1];
if (isset($argv)){
	//$port = $argv[2];
	//$browser = $argv[3];
	foreach ($argv as $key){
		if (strpos($key,"-u") !== false){	$url = substr($key,2);		}
		if (strpos($key,"-p") !== false){	$port = substr($key,2);		}
		if (strpos($key,"-b") !== false){	$browser = substr($key,2);	}
		if (strpos($key,"-a") !== false){	$adminurl = substr($key,2);	}
                if (strpos($key,"-s") !== false){       $http = substr($key,2);     }

	}
}

$xmlString = str_replace($oldUrl[0],$url,$xmlString);
$xmlString = str_replace($oldPort[0],$port,$xmlString);
$xmlString = str_replace($oldBrowser[0],$browser,$xmlString);
$xmlString = str_replace($oldAdminUrl[0],$adminurl, $xmlString);
$xmlString = str_replace("\"".$oldHttp[0]."\"","\"".$http."\"", $xmlString);


file_put_contents("TestSuite.xml",$xmlString);
