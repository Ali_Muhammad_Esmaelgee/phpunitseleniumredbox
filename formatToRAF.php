<?php
/**
 *  
 * @author Muhammad Esmaelgee <muhammad.esmaelgee@redboxdigital.com>
 * @copyright Copyright (c) 2015 Redbox Digital (http://www.redboxdigital.com)
 */


class formatter{



    public function __construct(){

        $filepath = $this->getFilePaths();

        foreach($filepath as $fileHtmlPath){

            echo $fileHtmlPath;
            $fileXml = $this->loadHtmlAsDom($fileHtmlPath);

        }
    }


    /**
     * @return SimpleXMLElement
     */
    public function loadHtmlAsDom($path){
        $html = file_get_contents($path);       //reads file into $html
        $doc = new DOMDocument();               //Initialise Dom Doc
        $doc->loadHTML($html);                  //load variable as html
        return simplexml_import_dom($doc);     //load dom elements
    }


    public function formatFiles(){

    }

    public function getFilePaths(){

        $projects = [];
        $filepath = [];
        exec("dir /b projects",$projects);

        foreach($projects as $project){

            $sep = DIRECTORY_SEPARATOR;
            $filenames = [];
            exec("dir /b projects" . $sep . $project . $sep . "seleniumIDE",$filenames);

            if(isset($filenames) && $filenames !== 0) {

                foreach($filenames as $file) {
                    if (strpos($file, ".html") > 0) {
                        array_push($filepath,"projects" . $sep . $project . $sep . "seleniumIDE" . $sep. $file);
                        //loadFile
                        //readFile
                        //formatFile
                        //exportFile
                    }
                }
            }
        }

        return $filepath;
    }



}

new formatter();